import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusComponent } from './components/status/status.component';
@NgModule({
  imports: [
CommonModule,
  ],

  declarations: [StatusComponent]
})
export class EmployeeModule { }
