import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Qualifier1Component } from './qualifier1.component';

describe('Qualifier1Component', () => {
  let component: Qualifier1Component;
  let fixture: ComponentFixture<Qualifier1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Qualifier1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Qualifier1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
