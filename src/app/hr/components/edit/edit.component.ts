import { Component, OnInit } from '@angular/core';
import { RecruitService } from '../../../recruit.service';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  recruit: any;
  angForm: FormGroup;
  constructor(private route: ActivatedRoute, private router: Router , private service: RecruitService, private fb: FormBuilder) {
    this.createForm();
   }

   createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      empID: ['', Validators.required ],
      role: ['', Validators.required ],
      gmail: ['', Validators.required ],
      password: ['', Validators.required ]

   });
  }

  updateRecruit(name, empID, role, gmail , password) {
    this.route.params.subscribe(params => {
    this.service.updateRecruit(name, empID, role, gmail ,password, params['id']);
    this.router.navigate(['show']);
  });
}


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.recruit = this.service.editRecruit(params['id']).subscribe(res => {
        this.recruit = res;
      });
    });
  }

}
