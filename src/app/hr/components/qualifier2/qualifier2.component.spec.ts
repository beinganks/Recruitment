import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Qualifier2Component } from './qualifier2.component';

describe('Qualifier2Component', () => {
  let component: Qualifier2Component;
  let fixture: ComponentFixture<Qualifier2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Qualifier2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Qualifier2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
