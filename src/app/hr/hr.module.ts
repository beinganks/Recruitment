import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListcandidatesComponent } from './components/listcandidates/listcandidates.component';
import { ShortlistComponent } from './components/shortlist/shortlist.component';
import { Qualifier1Component } from './components/qualifier1/qualifier1.component';
import { Qualifier2Component } from './components/qualifier2/qualifier2.component';
import { FinalisedComponent } from './components/finalised/finalised.component';
import { FormsModule,  FormBuilder,  Validators, ReactiveFormsModule } from '@angular/forms';
import { EditComponent } from './components/edit/edit.component';
import { AppRoutingModule } from '../app-routing.module';
import { HrdbComponent } from './components/hrdb/hrdb.component';
import { SuccessComponent } from './components/success/success.component';
@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    ListcandidatesComponent,
    ShortlistComponent,
    Qualifier1Component,
    Qualifier2Component,
    FinalisedComponent,
    ReactiveFormsModule,
    HrdbComponent,
    EditComponent,
    FormsModule
  ],
  declarations: [ListcandidatesComponent, ShortlistComponent, Qualifier1Component, Qualifier2Component, FinalisedComponent, EditComponent, HrdbComponent, SuccessComponent]
})
export class HrModule { }
