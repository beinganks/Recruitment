import { NgModule } from '@angular/core';
import { RouterModule, Routes  } from '@angular/router';
import { FinalisedComponent } from './hr/components/finalised/finalised.component';
import { ShortlistComponent } from './hr/components/shortlist/shortlist.component';
import { EditComponent } from './hr/components/edit/edit.component';
import { StatusComponent } from './employee/components/status/status.component';
import { HomeComponent } from './home/components/home/home.component';
import { TechdbComponent } from './interviewers/components/techdb/techdb.component';
import { HrdbComponent } from './hr/components/hrdb/hrdb.component';
import { AuthGuard } from './auth.guard';
import { AuthComponent } from './auth/auth/auth.component';



export const appRoutes: Routes = [
  { 
    path: 'edit/:id', 
    component:EditComponent,
    canActivate: [AuthGuard]

  },
  {
    path: 'login',
    component:AuthComponent 
  },
  { 
    path: 'hr',
    component:HrdbComponent,
    canActivate: [AuthGuard]

  },
  { 
    path: 'tech',
    component: TechdbComponent,
    canActivate: [AuthGuard]

  },
  { 
    path: 'employ',
    component:StatusComponent,
    canActivate: [AuthGuard]

  },
  { 
    path: 'home',
    component:HomeComponent,

  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { 
}
