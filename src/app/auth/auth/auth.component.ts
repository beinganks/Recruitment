import { Component, OnInit } from '@angular/core';
import { Subject } from '../../../../node_modules/rxjs';
import { RecruitService } from '../../recruit.service';
import { Router } from '../../../../node_modules/@angular/router';
import { LoginService } from '../login.service';
import { Validators, FormBuilder, NgForm } from '../../../../node_modules/@angular/forms';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  form;
  users : any;
  x;
  constructor(private fb: FormBuilder,
    private main :RecruitService,
    private logins : LoginService, 
    private router: Router) { 
    this.form = fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }
  submitlog ;
  isloggedin;

  login() {
    if (this.form.valid) {
      this.logins.sendToken(this.form.value.email)
      this.main.getRecruits().subscribe(res => {
        this.users = res;
        console.log(res);
      let getting = this.users.find((x)=>{
        return x.gmail=== this.form.value.email && x.password===this.form.value.password 
      });
      if(getting) 
      {
        if(getting.role == "Technical Interviewer" || getting.role =="General Interviewer" ){
          this.router.navigate(['/tech']);
        }
        if(getting.role == "Software Developer" || getting.role =="UI developer" || getting.role =="Backend developer" ){
          this.router.navigate(['/employ']);
        }
        if(getting.role == "HR" ){
          this.router.navigate(['/hr']);
        }
        return true;
      }
      return false;
    });

    }

  }

  ngOnInit() { }

}


 