import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from '../auth.guard';
import { LoginService } from './login.service';
@NgModule({
  imports: [
    CommonModule
  ],
  providers:[AuthGuard,LoginService],
  declarations: []
})
export class AuthModule { }
