import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechdbComponent } from './techdb.component';

describe('TechdbComponent', () => {
  let component: TechdbComponent;
  let fixture: ComponentFixture<TechdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
