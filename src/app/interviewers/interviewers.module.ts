import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TechdbComponent } from './components/techdb/techdb.component';
import { GetlistComponent } from './components/getlist/getlist.component';
import { JudgeComponent } from './components/judge/judge.component';
@NgModule({
  imports: [
    CommonModule
  ],
  exports:[
    GetlistComponent,
    TechdbComponent,
    JudgeComponent
  ],
  declarations: [GetlistComponent, JudgeComponent, TechdbComponent]
})
export class InterviewersModule { }
