import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule,  FormBuilder,  Validators, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { RecruitService } from './recruit.service';
import { FinalisedComponent } from './hr/components/finalised/finalised.component';
import { HomeComponent } from './home/components/home/home.component';
import { ShortlistComponent } from './hr/components/shortlist/shortlist.component';
import { EditComponent } from './hr/components/edit/edit.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { EmployeeModule } from './employee/employee.module';
import { HrdbComponent } from './hr/components/hrdb/hrdb.component';
import { TechdbComponent } from './interviewers/components/techdb/techdb.component';
import { AuthGuard } from './auth.guard';
import { AuthComponent } from './auth/auth/auth.component';
import { LoginService } from './auth/login.service';



@NgModule({
  declarations: [
AppComponent,
FinalisedComponent,
ShortlistComponent,
TechdbComponent,
EditComponent,
HomeComponent,
HrdbComponent,
AuthComponent,



  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    EmployeeModule,
  


    ],
  providers: [RecruitService,  AuthGuard, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }