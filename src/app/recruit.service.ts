import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { Observable } from '../../node_modules/rxjs';
import{map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecruitService {

  result: any;
  constructor(private http: HttpClient) { }

  addRecruit(name, empID,role, gmail,password) {
    const uri = 'http://localhost:4000/recruits/add';
    const obj = {
      name: name,
      empID:empID,
      role:role,
      gmail:gmail,
      password:password
    };
    this.http.post(uri, obj)
        .subscribe(res => console.log('Done'));
  }
  getRecruits() {
    const uri = 'http://localhost:4000/recruits';
    return this
            .http
            .get(uri).pipe(
              map(res => {
                return res;
              })
            )
            
  }

  editRecruit(id) {
    const uri = 'http://localhost:4000/recruits/edit/' + id;
    return this
            .http
            .get(uri)
            .pipe(
              map(res => {
                return res;
              })
            )
            
            
}

updateRecruit(name, empID, role,gmail,password, id) {
  const uri = 'http://localhost:4000/recruits/update/' + id;

  const obj = {
    name: name,
    empID: empID,
    role: role,
    gmail: gmail,
    password: password

  };
  this
    .http
    .post(uri, obj)
    .subscribe(res => console.log('Done'));
}

deleteRecruit(id) {
  const uri = 'http://localhost:4000/recruits/delete/' + id;

      return this
          .http
          .get(uri).pipe(
            map(res => {
              return res;
            })
          )
}

}