import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { CarrersComponent } from './components/carrers/carrers.component';
import { FormsModule } from '../../../node_modules/@angular/forms';
import { AuthGuard } from '../auth.guard';
import { LoginService } from '../auth/login.service';
import { AuthComponent } from '../auth/auth/auth.component';
import { FormComponent } from './components/form/form.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule

  ],
  exports:[
    HomeComponent,
    CarrersComponent,
    AuthComponent
  ],
providers:[AuthGuard, LoginService ],
  declarations: [HomeComponent, CarrersComponent, AuthComponent, FormComponent]
})
export class HomeModule { }
